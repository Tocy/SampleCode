
// 显示及设置控制台相关参数
// 1. 设置及获取控制台标题栏
// 2. 获取屏幕输出缓冲的参数
// 3. 设置控制台窗口大小
// 4. 设置控制台屏幕缓冲大小
// 建议使用visual studio 2005以上版本编译，使用unicode编码

#include <windows.h>
#include <iostream>
using std::wcout;
using std::endl;

int _tmain(int argc, _TCHAR* argv[])
{
	std::wcout.imbue(std::locale("chs"));

	// 获取和设置控制台标题栏
	DWORD title_need_size = 1024; // 64k
	TCHAR * console_title = new TCHAR[title_need_size+1];
	memset(console_title, 0, (title_need_size+1)*sizeof(TCHAR));
	DWORD title_size = GetConsoleTitle(console_title, title_need_size);
	if (0 == title_size)
	{
		wcout << "调用GetConsoleTitle异常，错误码"<< GetLastError() << endl;
		delete [] console_title;
		return 0;
	}
	wcout << _T("控制台标题栏") << console_title << endl;
	SetConsoleTitle(_T("ShowConsoleInfo"));
	wcout << endl << endl;

	HANDLE stdOutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	if (INVALID_HANDLE_VALUE == stdOutHandle)
	{
		wcout << "调用GetStdHandle失败，错误码" << GetLastError() << endl;
		return 0;
	}

	// 获取屏幕缓冲信息
	CONSOLE_SCREEN_BUFFER_INFO console_screen_buff_info;
	memset(&console_screen_buff_info, 0, sizeof(CONSOLE_SCREEN_BUFFER_INFO));
	if (GetConsoleScreenBufferInfo(stdOutHandle, &console_screen_buff_info))
	{
		wcout << _T("屏幕缓冲大小:列数x行数") << console_screen_buff_info.dwSize.X
			<< _T("x") << console_screen_buff_info.dwSize.Y << endl;
		wcout << _T("控制台光标位置(x,y):(") << console_screen_buff_info.dwCursorPosition.X
			<< _T(",") << console_screen_buff_info.dwCursorPosition.Y << _T(")") << endl;
		wcout << _T("屏幕缓冲字符属性: ") << std::hex << console_screen_buff_info.wAttributes << endl;
		wcout << _T("屏幕缓冲显示窗口位置left:") << console_screen_buff_info.srWindow.Left 
			<< _T(" Right:") << console_screen_buff_info.srWindow.Right 
			<< _T(" Top:") << console_screen_buff_info.srWindow.Top
			<< _T(" Bottom:") << console_screen_buff_info.srWindow.Bottom <<endl;
		wcout << _T("最大显示窗口大小:") << console_screen_buff_info.dwMaximumWindowSize.X
			<< _T("x") << console_screen_buff_info.dwMaximumWindowSize.Y << endl;
	}

	// 下面设置需要将控制台屏幕缓冲窗口下移三行，用于移除关于设置控制台标题提示
	SMALL_RECT sRect;
	sRect.Left = 0;
	sRect.Right = 0;
	sRect.Top = 3;
	sRect.Bottom = 3;
	if (!SetConsoleWindowInfo(stdOutHandle, FALSE, &sRect))
	{
		wcout << "调用SetConsoleWindowInfo失败，错误码" << GetLastError() << endl;
		return 0;
	}
	// 以上代码是控制台滚屏的一种实现方式，可考虑扩展下
	// msdn上的例子
	// http://msdn.microsoft.com/en-us/library/windows/desktop/ms685118(v=vs.85).aspx

	// 可使用SetConsoleScreenBufferSize来设置屏幕缓冲大小

	return 0;
}