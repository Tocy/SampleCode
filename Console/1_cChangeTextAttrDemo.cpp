// 控制台字体属性设置示例 ChangeTextAttrDemo
// 设置前景色、背景色、unicode下划线、删除线等
// 建议使用vs2005以上版本编译，unicode编码

#include <windows.h>
#include <iostream>
using std::wcout;
using std::endl;

int _tmain(int argc, _TCHAR* argv[])
{
	// 输出中文
	std::wcout.imbue(std::locale("chs"));

	// 设置控制台标题栏
	SetConsoleTitle(_T("ChangeTextAttrDemo"));

	HANDLE stdOutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	if (INVALID_HANDLE_VALUE == stdOutHandle)
	{
		wcout << L"调用GetStdHandle失败，错误码" << GetLastError() << endl;
		return 0;
	}

	// 默认背景色黑色，前景色白色
	wcout << _T("默认背景色，ABCabc123*&@!") << endl << endl;

	// 红色前景色，绿色背景色
	SetConsoleTextAttribute(stdOutHandle, FOREGROUND_RED|BACKGROUND_GREEN);
	wcout << _T("设置红色前景色，绿色背景色") << endl;
	wcout << _T("测试字符串，ABCabc123*&@!") << endl << endl;

	// 前景色加亮对比
	SetConsoleTextAttribute(stdOutHandle, 
		FOREGROUND_RED|BACKGROUND_GREEN|FOREGROUND_INTENSITY);
	wcout << _T("设置红色前景色，绿色背景色，前景色加亮对比") << endl;
	wcout << _T("测试字符串，ABCabc123*&@!") << endl << endl;

	// 设置为默认字体属性+上划线
	SetConsoleTextAttribute(stdOutHandle, 
		FOREGROUND_RED|FOREGROUND_BLUE|FOREGROUND_GREEN|COMMON_LVB_GRID_HORIZONTAL);
	wcout << _T("设置为默认字体属性+顶部水平网格") << endl;
	wcout << _T("测试字符串，ABCabc123*&@!") << endl << endl;

	// 设置为蓝色前景+下划线
	SetConsoleTextAttribute(stdOutHandle, 
		FOREGROUND_BLUE|COMMON_LVB_UNDERSCORE);
	wcout << _T("设置为蓝色前景+下划线") << endl;
	wcout << _T("测试字符串，ABCabc123*&@!") << endl << endl;

	// 设置为绿色前景+左划线
	SetConsoleTextAttribute(stdOutHandle, 
		FOREGROUND_GREEN|COMMON_LVB_GRID_LVERTICAL);
	wcout << _T("设置为绿色前景+左竖网格") << endl;
	wcout << _T("  测试字符串，ABCabc123*&@!") << endl << endl;

	// 设置为绿色背景+右划线
	SetConsoleTextAttribute(stdOutHandle, 
		BACKGROUND_GREEN|COMMON_LVB_GRID_RVERTICAL);
	wcout << _T("设置为绿色背景+右竖网格") << endl;
	wcout << _T("  测试字符串，ABCabc123*&@!") << endl << endl;

	SetConsoleTextAttribute(stdOutHandle, 
		FOREGROUND_RED|FOREGROUND_BLUE|FOREGROUND_GREEN);
	return 0;
}