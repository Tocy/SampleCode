// 控制台光标样式获取、设置，CursorAttrDemo
// 1. 光标属性获取及设置
// 2. 光标位置获取及设置
// 建议使用vs2005以上版本编译，unicode编码

#include <windows.h>
#include <iostream>
using std::wcout;
using std::endl;

int _tmain(int argc, _TCHAR* argv[])
{
	// 输出中文
	std::wcout.imbue(std::locale("chs"));

	// 设置控制台标题栏
	SetConsoleTitle(_T("CursorAttrDemo"));

	HANDLE stdOutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	if (INVALID_HANDLE_VALUE == stdOutHandle)
	{
		wcout << L"调用GetStdHandle失败，错误码" << GetLastError() << endl;
		return 0;
	}

	CONSOLE_CURSOR_INFO cursor_info;
	memset(&cursor_info, 0, sizeof(cursor_info));
	if (GetConsoleCursorInfo(stdOutHandle, &cursor_info))
	{
		wcout << _T("光标可见：") << (cursor_info.bVisible ? _T("是") : _T("否")) << endl;
		wcout << _T("光标占位比例 ：") << cursor_info.dwSize << _T("%") << endl;

		cursor_info.bVisible = TRUE;
		cursor_info.dwSize = 100;
		SetConsoleCursorInfo(stdOutHandle, &cursor_info);
		wcout << _T("设置100%光标占比") << endl;
		int x;
		std::cin >> x;

		cursor_info.dwSize = 50;
		SetConsoleCursorInfo(stdOutHandle, &cursor_info);
		wcout << _T("设置50%光标占比") << endl;
	}

	// 获取光标位置使用GetConsoleScreenBufferInfo函数
	// 直接输出定位到第二行
	COORD cursor_pos = {0, 8};
	SetConsoleCursorPosition(stdOutHandle, cursor_pos);
	
	return 0;
}