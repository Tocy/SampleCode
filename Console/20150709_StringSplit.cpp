// 20150709_StringSplit.cpp : 定义控制台应用程序的入口点。
// 建议使用控制台程序，多字节码编译

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;

// 唯一的应用程序对象

CWinApp theApp;

using namespace std;
void SplitUseTokenize(const char * source, char token);
void SplitUseExtract(const char * source, char token);
void SplitUseStrtok(char * source, char token);

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	// 初始化 MFC 并在失败时显示错误
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: 更改错误代码以符合您的需要
		_tprintf(_T("错误: MFC 初始化失败\n"));
		nRetCode = 1;
	}
	else
	{
		// TODO: 在此处为应用程序的行为编写代码。
		char source[] = "123\n\n456\n789";
		char token = '\n';
		SplitUseTokenize(source, token);
		SplitUseExtract(source, token);
		SplitUseStrtok(source, token);

		system("pause");
	}

	return nRetCode;
}

// 按照token分割source字符串，结果通过cout输出
// 使用MFC的CString::Tokenize
void SplitUseTokenize(const char * source, char token)
{

	CString strSource = source;
	CString strToken(token);

	int pos = 0;
		
	while (-1 != pos)
	{
		CString strCur = strSource.Tokenize(strToken, pos);

		if (!strCur.IsEmpty())
		{
			cout << strCur << endl;
		}
	}

	cout << endl << endl;
}

// 按照token分割source字符串，结果通过cout输出
// 使用MFC中的AfxExtractSubString函数
void SplitUseExtract(const char * source, char token)
{
	int pos = 0;
	CString strCur = "";
	while(AfxExtractSubString(strCur, source, pos, token))
	{
		++pos;
		cout << strCur << endl;
	}

	cout << endl << endl;
}

// 按照token分割source字符串，结果通过cout输出
// 使用crt的strtok函数
void SplitUseStrtok(char * source, char token)
{
	char * ptr = strtok(source, &token);
	while(NULL != ptr)
	{   
		cout << ptr << endl;
		ptr = strtok(NULL, &token);
	}
}
