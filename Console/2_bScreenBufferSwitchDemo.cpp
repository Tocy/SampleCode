// ��Ļ���崴�����л���ScreenBufferSwitchDemo
// 1. �����µ���Ļ���壬���л�
// 2. ����̨�������Unicode�ַ�
// ����ʹ��vs2005���ϰ汾���� unicode����
#include <windows.h> 
#include <iostream>
using std::wcout;
using std::endl;

int _tmain(int argc, _TCHAR* argv[])
{
	// �������
	std::wcout.imbue(std::locale("chs"));

	// ���ÿ���̨������
	SetConsoleTitle(_T("ScreenBufferSwitchDemo"));

	HANDLE stdOutHandle = GetStdHandle(STD_OUTPUT_HANDLE);

	HANDLE newConsoleHandle = CreateConsoleScreenBuffer(
		GENERIC_READ | GENERIC_WRITE, // ����Ȩ��
		FILE_SHARE_READ | FILE_SHARE_WRITE, // ��������
		NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
	if (INVALID_HANDLE_VALUE == newConsoleHandle)
	{
		wcout << TEXT("������Ļ����ʧ�ܣ�������") << GetLastError() << endl;
		return -1;
	}

	TCHAR arrStdText[] = TEXT("test screen buffer creation\n");
	DWORD writeCharNumbers = 0;
	WriteConsole(stdOutHandle, arrStdText, _tcslen(arrStdText), &writeCharNumbers, NULL);

	TCHAR arrNewText[] = TEXT("���Կ���̨���崴��\n");
	WriteConsole(newConsoleHandle, arrNewText, _tcslen(arrNewText), &writeCharNumbers, NULL);

	Sleep(2000);

	// �л����Ļ����
	if (!SetConsoleActiveScreenBuffer(newConsoleHandle))
	{
		wcout << TEXT("�л���Ļ����ʧ�ܣ�������") << GetLastError() << endl;
	}
	wcout << TEXT("����ʹ���´�������Ļ����") << endl;

	Sleep(2000);

	// �ָ�Ĭ�ϵı�׼��Ļ����
	if (!SetConsoleActiveScreenBuffer(stdOutHandle))
	{
		wcout << TEXT("�л���Ļ����ʧ�ܣ�������") << GetLastError() << endl;
	}
	wcout << TEXT("����ʹ�ñ�׼��Ļ����") << endl;

	// ע�� c/c++��׼���롢�������׼�����Ǻ�STD_OUTPUT_HANDLE������

	CloseHandle(newConsoleHandle);

	return 0;
}