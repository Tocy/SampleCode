// 按区域读写屏幕缓冲，ScreenBufferBlockDemo
// 从指定区域读取屏幕缓冲，然后将其输出到另一个屏幕缓冲上
// 建议使用vs2005以上版本编译 unicode编码
#include <windows.h> 
#include <iostream>
using std::wcout;
using std::endl;

int _tmain(int argc, _TCHAR* argv[])
{
	// 输出中文
	std::wcout.imbue(std::locale("chs"));

	// 设置控制台标题栏
	SetConsoleTitle(TEXT("ScreenBufferBlockDemo"));

	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
	HANDLE hNewScreenBuffer = CreateConsoleScreenBuffer( 
		GENERIC_READ |           // read/write access 
		GENERIC_WRITE, 
		FILE_SHARE_READ | 
		FILE_SHARE_WRITE,        // shared 
		NULL,                    // default security attributes 
		CONSOLE_TEXTMODE_BUFFER, // must be TEXTMODE 
		NULL);                   // reserved; must be NULL 
	if (hStdout == INVALID_HANDLE_VALUE || 
		hNewScreenBuffer == INVALID_HANDLE_VALUE) 
	{
		wcout << TEXT("CreateConsoleScreenBuffer failed - ") << GetLastError() << endl; 
		return 1;
	}

	// 设置标准缓冲的输出属性，并输出数据
	SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN);
	for(int i = 1; i < 16; ++i)
	{
		for (int j = 0; j < i; ++j)
		{
			wcout << TEXT("A");
		}
		wcout << endl;
	}

	// Make the new screen buffer the active screen buffer. 
	if (!SetConsoleActiveScreenBuffer(hNewScreenBuffer) ) 
	{
		printf("SetConsoleActiveScreenBuffer failed - (%d)\n", GetLastError()); 
		return 1;
	}

	SMALL_RECT srctReadRect;
	// Set the source rectangle.
	srctReadRect.Top = 2;    // top left: row 2, col 0 
	srctReadRect.Left = 0; 
	srctReadRect.Bottom = 6; // bot. right: row 6, col 79 
	srctReadRect.Right = 79; 

	COORD coordBufSize; 
	COORD coordBufCoord; 
	// The temporary buffer size is 2 rows x 80 columns.
	coordBufSize.Y = 5; 
	coordBufSize.X = 80; 

	// The top left destination cell of the temporary buffer is 
	// row 0, col 0. 
	coordBufCoord.X = 0; 
	coordBufCoord.Y = 0; 

	CHAR_INFO chiBuffer[5*80]; // [5][80]; 
	
	BOOL fSuccess; 
	// Copy the block from the screen buffer to the temp. buffer. 
	fSuccess = ReadConsoleOutput( 
		hStdout,        // screen buffer to read from 
		chiBuffer,      // buffer to copy into 
		coordBufSize,   // col-row size of chiBuffer 
		coordBufCoord,  // top left dest. cell in chiBuffer 
		&srctReadRect); // screen buffer source rectangle 
	if (!fSuccess) 
	{
		wcout << TEXT("ReadConsoleOutput failed - ") << GetLastError() << endl; 
		return 1;
	}

	SMALL_RECT srctWriteRect; 
	// Set the destination rectangle. 
	srctWriteRect.Top = 3;    // top lt: row 3, col 0 
	srctWriteRect.Left = 0; 
	srctWriteRect.Bottom = 7; // bot. rt: row 7, col 79 
	srctWriteRect.Right = 79; 

	// Copy from the temporary buffer to the new screen buffer. 
	fSuccess = WriteConsoleOutput( 
		hNewScreenBuffer, // screen buffer to write to 
		chiBuffer,        // buffer to copy from 
		coordBufSize,     // col-row size of chiBuffer 
		coordBufCoord,    // top left src cell in chiBuffer 
		&srctWriteRect);  // dest. screen buffer rectangle 
	if (!fSuccess) 
	{
		wcout << TEXT("WriteConsoleOutput failed - ") << GetLastError() << endl;
		return 1;
	}
	Sleep(5000); 

	// Restore the original active screen buffer. 

	if (!SetConsoleActiveScreenBuffer(hStdout)) 
	{
		wcout << TEXT("SetConsoleActiveScreenBuffer failed - ") << GetLastError() << endl; 
		return 1;
	}

	CloseHandle(hNewScreenBuffer);
	
	return 0;
}