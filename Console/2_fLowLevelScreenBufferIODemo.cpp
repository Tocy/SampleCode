// 读写屏幕缓冲底层接口，LowLevelScreenBufferIODemo
// 向屏幕缓冲读写字符串或字符数组
// 建议使用vs2005以上版本编译 unicode编码
#include <windows.h> 
#include <iostream>
using std::wcout;
using std::endl;

int _tmain(int argc, _TCHAR* argv[])
{
	// 输出中文
	std::wcout.imbue(std::locale("chs"));

	// 设置控制台标题栏
	SetConsoleTitle(TEXT("LowLevelScreenBufferIODemo"));

	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);

	// 设置标准缓冲的输出属性，并输出数据
	SetConsoleTextAttribute(hStdout, FOREGROUND_RED|BACKGROUND_GREEN);
	wcout << TEXT("abcdefg空山不见人，") << endl
		<< TEXT("hijklmn但闻人语响。") << endl
		<< TEXT("opqrst返景入深林，") << endl
		<< TEXT("uvwxyz复照青苔上。") << endl;

	// 去掉背景色，用于区分后续输出属性字符是否正确拷贝了
	SetConsoleTextAttribute(hStdout, FOREGROUND_RED|FOREGROUND_GREEN);

	// 从屏幕缓冲(0,6)读入10个UNICODE字符
	TCHAR readStr[10] = {0};
	DWORD readTextNumber = 10;
	COORD coordRead = {6,0};
	DWORD actualUsedCount = 0;
	if (!ReadConsoleOutputCharacter(hStdout,
		readStr, readTextNumber, coordRead, &actualUsedCount))
	{
		wcout << TEXT("ReadConsoleOutputCharacter failed with ") << GetLastError() << endl;
	}
	else
	{
		// 数据写到屏幕缓冲的第6行起始位置
		COORD coordWrite = {0,5};
		if (!WriteConsoleOutputCharacter(hStdout,
			readStr, actualUsedCount, coordWrite, &actualUsedCount))
		{
			wcout << TEXT("WriteConsoleOutputCharacter failed with ") << GetLastError() << endl;
		}
	}

	// 这里仅读取了屏幕缓冲的字符属性
	WORD chiBuffer[10]; 
	coordRead.X = 5;
	coordRead.Y = 1;
	DWORD readCharInfoNumber = 10;
	if (!ReadConsoleOutputAttribute(hStdout, chiBuffer, readCharInfoNumber,
		coordRead, &actualUsedCount))
	{
		wcout << TEXT("ReadConsoleOutputAttribute failed with ") << GetLastError() << endl;
	}
	else
	{
		// 数据写到第7行起始位置
		COORD coordWrite = {0,6};
		if (!WriteConsoleOutputAttribute(hStdout, chiBuffer, actualUsedCount,
			coordWrite, &actualUsedCount))
		{
			wcout << TEXT("WriteConsoleOutputAttribute failed with ") << GetLastError() << endl;
		}
	}

	// 在第7行起始位置填充中文"水"八次
	COORD coordFillChar = {0,6};
	if (!FillConsoleOutputCharacter(hStdout, 
		TEXT('水'), 8*sizeof(TCHAR), coordFillChar, &actualUsedCount))
	{
		wcout << TEXT("FillConsoleOutputCharacter failed with ") << GetLastError() << endl;
	}
	Sleep(2000);
	FillConsoleOutputAttribute(hStdout, FOREGROUND_BLUE, 20, coordFillChar,
		&actualUsedCount);

	return 0;
}