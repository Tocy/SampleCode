// CaptureApiTest.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "CaptureApiTest.h"
#include "GdiCapture.h"
#include "Dx9Capture.h"

#include <MMSystem.h>
#pragma comment(lib, "winmm.lib")

#include <cstdio>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 唯一的应用程序对象

CWinApp theApp;

using namespace std;

void GdiTest(const int count);
void Dx9Test(const int count);

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	// 初始化 MFC 并在失败时显示错误
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: 更改错误代码以符合您的需要
		_tprintf(_T("错误: MFC 初始化失败\n"));
		nRetCode = 1;
	}
	else
	{
		// TODO: 在此处为应用程序的行为编写代码。

		const int TEST_FRAME_TOTAL = 25*60;

		GdiTest(TEST_FRAME_TOTAL);

		//Dx9Test(TEST_FRAME_TOTAL);

		system("pause");

	}

	return nRetCode;
}

void GdiTest(const int count)
{
	//CaptureScreenshotbyGdi();

	Screenshot::GdiCapture gdiCap;
	gdiCap.Init();

	//Sleep(5000);

	printf("gdi capture test start\n");

	DWORD startTime = timeGetTime();
	//for (int i = 0; i < count; ++i)
	{
		gdiCap.CaptureFrame();
	}
	DWORD endTime = timeGetTime();

	printf("gdi capture %d need time %dms\n\n", count, endTime - startTime);
	// in 1200ms finished 1500 frames

	gdiCap.Deinit();
}

void Dx9Test(const int count)
{
	Screenshot::Dx9Capture dx9Cap;
	dx9Cap.Init(/*GetConsoleWindow()*/GetDesktopWindow());

	printf("dx9Front capture test start\n");

	DWORD startTime = timeGetTime();
	//for (int i = 0; i < count; ++i)
	{
		//dx9Cap.FrontBufferFrame();
		//dx9Cap.RenderTargetFrame();
		dx9Cap.BackBufferFrame();
		//Sleep(1);
	}
	DWORD endTime = timeGetTime();

	printf("dx9Front capture %d need time %dms\n\n", count, endTime - startTime);
	// 240+s, unaccepted

	dx9Cap.Uninit();
}
