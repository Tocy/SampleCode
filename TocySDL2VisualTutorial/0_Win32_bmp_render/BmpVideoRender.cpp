
#include "stdafx.h"
#include "BmpVideoRender.h"

BmpVideoRender::BmpVideoRender()
	: SDLVideoRender()
	, m_bmp_surface(nullptr)
{

}
BmpVideoRender::~BmpVideoRender()
{
	Deinit();
}

bool BmpVideoRender::Init(HWND show_wnd, RECT show_rect)
{
	if (!SDLVideoRender::Init(show_wnd, show_rect))
	{
		return false;
	}

	m_bmp_surface = SDL_LoadBMP("hello_world.bmp");
	if (nullptr == m_bmp_surface)
	{
		return false;
	}

	return true;
}
void BmpVideoRender::Deinit()
{
	if (nullptr != m_bmp_surface)
	{
		SDL_FreeSurface(m_bmp_surface);
		m_bmp_surface = nullptr;
	}

	SDLVideoRender::Deinit();
}

bool BmpVideoRender::Render()
{
	if (nullptr != m_bmp_surface)
	{
		SDL_Surface * window_surface = SDL_GetWindowSurface(m_sdl_window);
		SDL_BlitScaled(m_bmp_surface, NULL, window_surface,  &m_show_rect);

		SDL_UpdateWindowSurfaceRects(m_sdl_window, &m_show_rect, 1);
	}

	return true;
}