
#include "SDLVideoRender.h"

class BmpVideoRender: SDLVideoRender
{
public:
	BmpVideoRender();
	~BmpVideoRender();

	bool Init(HWND show_wnd, RECT show_rect);
	void Deinit();

	// width x height resolution
	// data[] for Y\U\V, stride is linesize of each raw
	void Update(int width, int height, unsigned char *data[3], int stride[3]){}
	bool Render();

private:
	SDL_Surface * m_bmp_surface;
	
};