
#include <SDL.h>
//#include <SDL_main.h>
#pragma comment(lib, "SDL2.lib")

class SDLVideoRender
{
public:
	SDLVideoRender();
	virtual ~SDLVideoRender();

	virtual bool Init(HWND show_wnd, RECT show_rect);
	virtual void Deinit();

	// width x height resolution
	// data[] for Y\U\V, stride is linesize of each raw
	virtual void Update(int width, int height, unsigned char *data[3], int stride[3]) = 0;
	virtual bool Render() = 0;

protected:
	SDL_Window * m_sdl_window;
	SDL_Rect m_show_rect;
};