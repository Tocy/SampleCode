#pragma once
#include "sdlvideorender.h"

class YuvRender :public SDLVideoRender
{
public:
	YuvRender(void);
	~YuvRender(void);

	bool Init(HWND show_wnd, RECT show_rect);
	void Deinit();

	// width x height resolution
	// data[] for Y\U\V, stride is linesize of each raw
	void Update(int width, int height, unsigned char *data[3], int stride[3]){}
	bool Render();

private:
	void FillTexture1();
	void FillTexture2();

private:
	int m_yuv_width;
	int m_yuv_height;
	int m_yuv_frame_size;
	unsigned char * m_yuv_data;

	SDL_Texture * m_show_texture;
};

