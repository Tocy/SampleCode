
// 2_sdl_yuv_player.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号


// CMy2_sdl_yuv_playerApp:
// 有关此类的实现，请参阅 2_sdl_yuv_player.cpp
//

class CMy2_sdl_yuv_playerApp : public CWinApp
{
public:
	CMy2_sdl_yuv_playerApp();

// 重写
public:
	virtual BOOL InitInstance();

// 实现

	DECLARE_MESSAGE_MAP()
};

extern CMy2_sdl_yuv_playerApp theApp;