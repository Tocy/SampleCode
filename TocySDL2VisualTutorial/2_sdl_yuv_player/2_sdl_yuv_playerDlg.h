
// 2_sdl_yuv_playerDlg.h : 头文件
//

#pragma once

#include "SDLRender/YuvRender.h"

// CMy2_sdl_yuv_playerDlg 对话框
class CMy2_sdl_yuv_playerDlg : public CDialogEx
{
// 构造
public:
	CMy2_sdl_yuv_playerDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_MY2_SDL_YUV_PLAYER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonPlay();
	DECLARE_MESSAGE_MAP()

private:
	bool InitRender(CString file_path);
	void DeinitRender();

private:
	UINT m_width;
	UINT m_height;
	UINT m_fps;

	FILE * m_in_file;

	unsigned char * m_frame_data;
	UINT m_frame_length; // w*h*1.5
	UINT m_plane_size; // w*h

	YuvRender m_yuv_render;
public:
	
};
