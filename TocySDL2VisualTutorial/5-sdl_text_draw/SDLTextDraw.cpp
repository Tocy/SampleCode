/*
* This is a file in SampleCode project.
* All rights reserved @ Tocy(Zhang Yujie) 2017
*/


#include "SDLTextDraw.h"
#include <stdio.h>


#define LOGD printf
#define LOGE printf

SDLTextDraw::SDLTextDraw()
	: mFont(NULL), mPointSize(DEFAULT_POINT_SIZE)
	, mTexture(NULL)
	//, mTTFPath("arial.ttf")
	, mTTFPath("simsun.ttc") // 宋体，支持中文和英文
{

}


SDLTextDraw::~SDLTextDraw()
{
	uninit();
}

bool SDLTextDraw::init(int flags)
{
	if (NULL != mFont)
		return true;
	
	if (!TTF_WasInit())
	{
		LOGE("%s %d TTF was not inited\n", __FUNCTION__, __LINE__);
		return false;
	}

	TTF_Font * font = TTF_OpenFont(mTTFPath.c_str(), mPointSize);
	if (NULL == font) {
		LOGE("%s %d Couldn't load %d pt font from %s error: %s\n",
			__FUNCTION__, __LINE__, mPointSize, mTTFPath.c_str(), SDL_GetError());
		return false;
	}
	// 字体显示参数设置
	//TTF_SetFontStyle(font, renderstyle);
	//TTF_SetFontOutline(font, outline);
	//TTF_SetFontKerning(font, kerning);
	//TTF_SetFontHinting(font, hinting);
	mFont = font;

	return NULL != mFont;
}

void SDLTextDraw::uninit()
{
	if (NULL != mTexture)
	{
		SDL_DestroyTexture(mTexture);
		mTexture = NULL;
	}
	if (NULL != mFont)
	{
		TTF_CloseFont(mFont);
		mFont = NULL;
	}
}

bool SDLTextDraw::update(Uint16 * text, SDL_Rect rect)
{
	SDL_Color color_black = { 0,0,0 };
	SDL_Surface * surface = TTF_RenderUNICODE_Solid(mFont, text, color_black);
	//TTF_RenderGlyph_Solid,

	if (NULL != surface)
	{
		mTexture = SDL_CreateTextureFromSurface((SDL_Renderer *)mRenderer, surface);
		SDL_FreeSurface(surface);
	}
	else
		return false;

	if (NULL != mTexture)
	{
		/* Clear the background to background color */
		//SDL_SetRenderDrawColor(mRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		//SDL_RenderClear(mRenderer);

		int w, h;
		SDL_QueryTexture(mTexture, NULL, NULL, &w, &h);
		rect.w = w;
		rect.h = h;
		SDL_RenderCopy((SDL_Renderer *)mRenderer, mTexture, NULL, &rect);
		//SDL_RenderPresent(renderer);
	}
	return true;
}

bool SDLTextDraw::update(char * text, SDL_Rect rect)
{
	if (NULL == text || '\0' == text)
	{
		LOGE("%s %d null show text\n", __FUNCTION__, __LINE__);
		return false;
	}

	if (SDL_TRUE == SDL_RectEmpty(&rect))
	{
		LOGE("%s %d empty show rect\n", __FUNCTION__, __LINE__);
		return false;
	}

	if (NULL == mRenderer)
		return false;

	mShowText = text;
	SDL_Color color_black = { 0,0,0 };
	//SDL_Surface * surface = TTF_RenderText_Solid(mFont, text, color_black);
	SDL_Surface * surface = TTF_RenderUTF8_Solid(mFont, text, color_black);
	//SDL_Surface * surface = TTF_RenderUNICODE_Solid(mFont, text, color_black);
	//TTF_RenderGlyph_Solid,

	if (NULL != surface)
	{
		mTexture = SDL_CreateTextureFromSurface((SDL_Renderer *)mRenderer, surface);
		SDL_FreeSurface(surface);
	}
	else
		return false;

	if (NULL != mTexture)
	{
		/* Clear the background to background color */
		//SDL_SetRenderDrawColor(mRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		//SDL_RenderClear(mRenderer);

		int w, h;
		SDL_QueryTexture(mTexture, NULL, NULL, &w, &h);
		rect.w = w;
		rect.h = h;
		SDL_RenderCopy((SDL_Renderer *)mRenderer, mTexture, NULL, &rect);
		//SDL_RenderPresent(renderer);
	}
	return true;
}
