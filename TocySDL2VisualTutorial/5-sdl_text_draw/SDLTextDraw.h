/*
* This is a file in SampleCode project.
* All rights reserved @ Tocy(Zhang Yujie) 2017
*/

#pragma once
#include "SDL.h"
#include "SDL_ttf.h"
#include <string>

class SDLTextDraw
{
public:
	SDLTextDraw();
	~SDLTextDraw();

	void set(SDL_Renderer * renderer)
	{
		mRenderer = renderer;
	}
	bool init(int flags);
	void uninit();

	// 这是一个依赖于SDL_Renderer的接口，这里仅更新renderer，不做刷新及其它显示处理
	bool update(char * text, SDL_Rect rect);// latin/utf-8
	bool update(Uint16 * text, SDL_Rect rect);// unicode

private:
	enum 
	{
		DEFAULT_POINT_SIZE = 18, // 默认字体大小
	};
private:
	TTF_Font * mFont;
	int mPointSize;
	SDL_Texture * mTexture;
	SDL_Renderer * mRenderer;

	std::string mTTFPath;
	std::string mShowText;
};

