/*
 * This is a file in SampleCode project.
 * All rights reserved @ Tocy(Zhang Yujie) 2017
*/

/* 本工程依赖于4_ttf及其资源，请先简单了解其中的资源构成和功能
 * 注意本demo仅仅是示例代码，没有复杂的逻辑。
 * 若需要在windows下显示中文字符，你需要将编码格式转为UNICODE
 * 若需要在linux下显示中文字符，你需要将编码格式转为UTF-8
 * 如果只有英文或者Latin字符，ASCII即可
*/
#include <stdio.h>

#include "SDLTextDraw.h"

#define WIDTH   640
#define HEIGHT  480

#define DEFAULT_TEXT    "The quick brown fox jumped over the lazy dog"
// 在Linux下是UTF-8可以直接输出
#define CHI_TEXT "来自中国的SDL文本测试123-+"
//=Unicode 编码:你好，Windows下只能使用Unicode编码
Uint16 win_msg[1024] = { 0x4F60,0x597D, 's', 'd', 'l', ' ', 'f', 'r', 'o','m', ' ', 't', 'o', 'c', 'y',0 }; 
void cleanup(int exitcode);
void draw(SDL_Renderer *renderer, SDLTextDraw &text, SDL_Rect &show_rect)
{
	/* Clear the background to background color */
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(renderer);
	static int selected = 1;
	if (1 == selected)
	{
		text.update(win_msg, show_rect);
		selected = 0;
	}
	else
	{
		text.update(DEFAULT_TEXT, show_rect);
	}
	
	SDL_RenderPresent(renderer);
}

#ifdef main
#undef main
#endif

int main(int argc, char * argv[])
{

	// 初始化TTF库
	if (TTF_Init() < 0) {
		fprintf(stderr, "Couldn't initialize TTF: %s\n", SDL_GetError());
		SDL_Quit();
		return(2);
	}

	SDL_Window * window = NULL;
	SDL_Renderer * renderer = NULL;
	// 创建SDL窗口
	if (SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, 0, &window, &renderer) < 0) {
		fprintf(stderr, "SDL_CreateWindowAndRenderer() failed: %s\n", SDL_GetError());
		cleanup(2);
	}

	SDLTextDraw text;
	text.set(renderer);
	text.init(0);

	SDL_Rect show_rect = {0,0, WIDTH, HEIGHT};
	draw(renderer, text, show_rect);

	/* 处理键盘和鼠标消息 */
	bool done = false;
	SDL_Event event;
	while (!done) {
		if (SDL_WaitEvent(&event) < 0) {
			fprintf(stderr, "SDL_PullEvent() error: %s\n",
				SDL_GetError());
			done = true;
			continue;
		}
		switch (event.type) {
		case SDL_MOUSEBUTTONDOWN:
			show_rect.x = event.button.x;
			show_rect.y = event.button.y;
			//show_rect.w = WIDTH;
			//show_rect.h = HEIGHT;
			draw(renderer, text, show_rect);
			break;

		case SDL_KEYDOWN:
		case SDL_QUIT:
			done = true;
			break;
		default:
			break;
		}
	}

	// 资源销毁及释放
	text.uninit();
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	cleanup(0);

	return 0;
}


void cleanup(int exitcode)
{
	TTF_Quit();
	SDL_Quit();
	exit(exitcode);
}