/* note my boost install path is "/d/dev-util/boost_1_59_0/"
 * compile in gcc 4.8.1
 * g++ 1_timer.cpp -I/d/dev-util/boost_1_59_0 -o 1_timer.exe
*/

#include <iostream>
#include "boost/timer.hpp"
using namespace boost;
using namespace std;

int main(int argc, char * argv[])
{
	timer t;
	cout << "max timespan: " << t.elapsed_max() / 3600 << "h" << endl;
	cout << "min timespan: " << t.elapsed_min() << "s" << endl;
	
	cout << "now time elapsed: " << t.elapsed() << "s" << endl;
	
	return 0;
}
