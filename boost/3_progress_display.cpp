/* note my boost install path is "/d/dev-util/boost_1_59_0/"
 * compile with gcc 4.8.1
 * g++ progress_display.cpp -I/d/dev-util/boost_1_59_0 -o progress_display.exe
 * 
*/

#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include "boost/progress.hpp"
using namespace boost;
using namespace std;

int main(int argc, char * argv[])
{
	vector<string> v(100);
	progress_display pd(v.size());
	for (int i = 0; i < v.size(); ++i)
	{
		++pd;
		if (0 == (i % 10)) for(int j = 0; j < 1000000; ++j) sqrt(35.7);
	}
	
	return 0;
}
