/* note my boost install path is "/c/project/opensource/008boost/boost_1_60_0"
 * compile with gcc 4.8.1
 * g++ 4_date_time.cpp -I/c/project/opensource/008boost/boost_1_60_0 -o date_time.exe
 * this is a demo to show the usage of progress_timer
 * 
*/

#include <iostream>
#include <cassert>
#define BOOST_DATE_TIME_SOURCE
#include "boost/date_time/gregorian/gregorian.hpp"
#include <boost/date_time/date_formatting.hpp>
#include <boost/date_time/gregorian/greg_month.hpp>
using namespace std;
using namespace boost::gregorian;

int main(int argc, char * argv[])
{
	date d1; // invalid date
	date d2(2016, 1, 22); // use numeric date
	date d3(2016, Jan, 1); // use alias date
	date d4(d2);
	
	assert(d1 == date(not_a_date_time));
	assert(d2 == d4);
	assert(d3 < d4);
	cout << "d1:" << d1 << endl;
	cout << "d2:" << d2 << endl;
	cout << "d3:" << d3 << endl;
	cout << "d4:" << d4 << endl;
	
/* 	date d5 = from_string("2016-05-01");
	date d6(from_string("2016/05/01"));
	date d7 = from_undelimited_string("20160601");
	cout << "d5:" << d1 << endl;
	cout << "d6:" << d2 << endl;
	cout << "d7:" << d3 << endl; */
	
	{
		date d(2016, 4,10);
		assert(d.year() == 2016);
		assert(d.month() == 4);
		assert(d.day() == 10);
		
		cout << "test d attr succ\n";
		
		date::ymd_type ymd = d.year_month_day();
		assert(ymd.year == 2016);
		assert(ymd.month == 4);
		assert(ymd.day == 10);
	}
	
	return 0;
}