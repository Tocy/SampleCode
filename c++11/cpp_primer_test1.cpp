/* compile in gcc 4.8.1
 * g++ -std=c++11 cpp_primer_test1.cpp
*/
#include <iostream>
#include <string>

void test_alias()
{
  // alias declarations
  using VInt = int; // type alias
  using PInt = int *; // pointer alias
  using RInt = int &; // reference alias
  using MyFunc = int (*)(void); // function pointer alias
  
  VInt i = 99;
  PInt pi = &i;
  RInt ri = i;
  MyFunc func = nullptr;
}

void test_auto()
{
  /* auto specifier */
  // a_i is int, a_pi is a pointer to int, a_ri is a reference to int
  auto a_i = 0, *a_pi = &a_i, &a_ri = a_i;
  // error like this, for a_f is float while a_d is double
  //auto a_f = 0.0f, a_d = 0.0;
}

void test_decltype()
{
  /*decltype specifier*/
  const int ci = 0, &cr = ci, *cp = &ci;
  decltype(ci) x = 0;// x has type const int
  decltype(cr) y = x;// y has type const int &
  //decltype(cr) z;// error,z is a reference and must be initialized

  decltype(cr+0) b;// b has type int
  //decltype(*cp) c;// error, c is a reference and must be initialized
  
  int i = 100;
  decltype(i) d;// d has type int
  //decltype((i)) e;// error ,e is a reference and must be initialized
}

void test_rangefor()
{
  /* range for */
  using std::string;
  string str("UPPER test");
  for (auto &c : str)
	  c = tolower(c);
}

#include <vector>
void test_vector()
{
	using std::vector;
	using std::string;
	/* vector */
	vector<vector<int>> ivec;
	
	// list initialization
	vector<string> strvec{"a", "bc", "def"};
	vector<string> v2{"123"};  
	vector<string> v3{5};
	vector<string> v4{5, "s"};
}



int main(int argc, char ** argv)
{
  test_alias();
  test_auto();
  test_decltype();
  test_rangefor();
  test_vector();
    
  return 0;
}

