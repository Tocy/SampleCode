/* compile in MinGW-W64 5.2.0
 * g++ -std=c++11 cpp_user_def_literal.cpp
*/

#include <iostream>
#include <complex>
#include <string>
using namespace std; // not recommended

namespace LT
{
	string operator""s(const char * sz, size_t n)
	{
		return string{sz, n};
	}
	
	constexpr complex<double> operator""i(long double d)
	{
		return {0.0, d};
	}
	
	template<char...>
	constexpr int operator"" _b3();
}

int main(int argc, char * argv[])
{
	using namespace LT;
	
	auto cp = 1.6i;
	auto str = "test literal"s;
	
	return 0;
}