/* compile in gcc 4.8.1
 * g++ -std=c++11 thread_util_sample_atomic.cpp
*/

// atomic::operator++ example
#include <iostream>       // std::cout
#include <atomic>         // std::atomic
#include <thread>         // std::thread
#include <vector>         // std::vector

std::atomic<int> ready{0};

void AtomicIncreasement (int id) 
{
  ++ready;
};

int main(int argc, char** argv)
{
  std::vector<std::thread> threads;
  std::cout << "spawning 5 threads that do incereasement...\n";
  for (int i=1; i<=5; ++i) threads.push_back(std::thread(AtomicIncreasement,i));
  for (auto& th : threads) th.join();
  
  std::cout << ready << std::endl;

  return 0;
}

