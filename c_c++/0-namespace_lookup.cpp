
// compile in gcc 5.2.0(x86-64)
// g++ 0-namespace_lookup.cpp

#include <iostream>
namespace Ns
{
	// inner X
	struct X
	{
		int m_a;
		float m_b;
	};
	
	void f(X x)
	{
		std::cout << "invoke Ns::f() \n";
	}
}

// global
struct X
{
	bool m_r;
	bool m_s;
};

void f(X x)
{
	std::cout << "invoke golbal f() \n";
}

int main(int argc, char* argv[])
{
	Ns::X in_x;
	X g_x;
	f(in_x);
	f(g_x);
	
	return 0;
}