/* 
 * Invoke c++ function and class in C Language
 * Copyright (c) Tocy <zyvj@qq.com>
*/

#include "1-c-invoke-cpp.h"
#include <cstdio>
#include <cstdlib>
//#include <iostream>

void DenyFunc()
{
	printf("You can\'t access c++ function in c directly.\n");
}

int FunCppDecorate(int param)
{
	printf("decorating by extern c, you have rights to invoke cpp function in c\nwith input %d\n"
			, param);
	return (param + 1);
}

/* Let's try to invoke overloading functoin*/
void OverloadFunc(int param, bool is_c)
{
	if (!is_c)
		printf("You can\'t access c++ overload int function in c directly. %d\n", param+1);
	else
		printf("You can access c++ overload int function by extern c decoration. %d\n", param+2);
}

void OverloadDecorate_i(int param)
{
	OverloadFunc(param, true);
}

void OverloadFunc(double param, bool is_c)
{
	if (!is_c)
		printf("You can\'t access c++ overload double function in c directly. %lf\n", param+3.0);
	else
		printf("You can access c++ overload double function by extern c decoration. %lf\n", param+6.0);
}

void OverloadDecorate_d(double param)
{
	OverloadFunc(param, true);
}

/* Next is a class  member function definition and invoke  */

AType::AType(){}
AType::~AType(){}
void AType::MemFunc(int value)
{
	printf("You are in AType::MemFunc %d\n", value);
}

extern "C" struct TagAType
{
	AType a;
};

struct TagAType * CreateInstance()
{
	return (TagAType*)malloc(sizeof(TagAType));
}
void DestoryInstance(struct TagAType ** atype) 
{
	if (NULL != atype && NULL != *atype)
	{
		free(*atype);
		atype = NULL;
	}
}
void ClassMemFunc(struct TagAType * pthis, int param)
{
	if(NULL != pthis)pthis->a.MemFunc(param);
}
