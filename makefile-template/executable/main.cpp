/* 
 * Excutable Makefile test
 * Copyright (c) Tocy <zyvj@qq.com>
 */

# include <iostream>

int main(int argc, char ** argv)
{
    using std::cout;
    using std::endl;

    cout << argv[0] << ", welcome to exe makefile test!" << endl;

    return 0;
}