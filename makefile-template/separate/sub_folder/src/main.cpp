/* 
 * Excutable Makefile test
 * 将源码与目标文件分开
 * separate source code with object files in diffrent folder
 * Copyright (c) Tocy <zyvj@qq.com>
 */

# include <iostream>

#include "audio/audio.h"
#include "video/video.h"

int main(int argc, char ** argv)
{
    using std::cout;
    using std::endl;

    cout << argv[0] << ", welcome to exe makefile test!" << endl;
	
	audio_init();
	video_init();

    return 0;
}